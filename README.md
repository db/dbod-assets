# DBOD Assets

### Usage

- Always prefer the standard logos over other colors. The standard logos are meant to be used on light to medium shades backgrounds.
- If you are using the logo on a dark background, go for the white colored ones.
- In the white colored logos, prefer the ones without outlines (named *without* added suffixes `_2`).
- The most used formats are the `logo.png` and `logo_with_title.png`. Both of these are provided in square dimensions for ease of use.
- There should be sufficient clear space around the logo.
- DBOD logos are provided for as-is usage. Do not modify the color, font or graphics of the images without prior permission.
